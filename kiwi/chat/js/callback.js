/**

    SurveyView is a view object that is responsible
    for showing and submitting the survey form.

**/
App.CallBackView = Backbone.View.extend({

    className : 'eg-chat-callback',

    template : _.template($('#tpl-callback').html()),

    events : {

        'submit form' : 'onSubmitForm',
		'onload' : 'hideAddBar',
		'orientationchange' : 'hideAddBar'
    },

    initialize : function() {
       
    },

    render : function() {
		var height = eGainLiveConfig.callBackWindowHeight;
		if($.browser.msie && $.browser.version == '7.0')
			height+=10;
		else if($.browser.msie && $.browser.version == '9.0')
			height-=10;
		else if($.browser.mozilla)
			height-=10;
		if(!(CONFIG_KNOWLEDGE_BASE_URL && CONFIG_KNOWLEDGE_BASE_URL.length>0)){	
			height+=53;
		}
		var d = getDimensions('');
		if(App.offerConstraints.size){
			d.width = App.offerConstraints.size.width;
			height = App.offerConstraints.size.height;
		}
		if(top && top.resizeTo)
			top.resizeTo(d.width,height);
        this.$el.html(this.template());
		if($.browser.msie && $.browser.version == "7.0")
			this.$el.find('.submit-section').height('50px');
        

 		$('#eg-chat-header a.closechat').attr('title',L10N_WINDOW_CLOSE_BUTTON);

        $('#eg-chat-content').css('width','100%');
		$('#eg-chat-header').width('100%');
        $('#eg-chat-content').html(this.$el);
		
		 $(window).on('resize.egain.callback', _.bind(this.onWindowResize, this));
     if(!App.utils.isVisitorMobile()) {		
		 this.$el.find('.form-fields').tinyscrollbar({ sizethumb: App.scrollsize });
		}
		this.resize();
		if(App.utils.isVisitorIos())
		{App.utils.hideAddressBar();}
    },

     hideAddBar :function(){
	  if(App.utils.isVisitorIos())
	   App.utils.hideAddressBar();
	 },
    onSubmitForm : function(e) {

        e.preventDefault();

        
		$(window).off('.egain.survey');

    },

    onWindowResize : function(e) {

        this.resize();
    },

    resize : function() {

        var windowHeight = $(window).height();
		var windowWidth = $(window).width();
        var topOffset = this.$el.offset().top;

		this.$('.submit-section').width(windowWidth-App.pageOffset);
		
		if(App.utils.isVisitorMobile()){
			var $form = this.$el.find('.form-fields');
			this.$el.height('auto');
			var height = $form.find('.viewport .overview').height();
			
			
			$form.height(height);
			$form.find('.viewport').height(height);
			
			$('#eg-chat-content').addClass('Nofloating-submit-section');
		}
		else{
			this.$el.height(windowHeight-App.submitSectionHeight-topOffset);
			var $form = this.$el.find('.form-fields');
			var formOffset = $form.offset().top;
			var submitTop = $('.submit-section').position().top;
			var height = submitTop-formOffset;
			$form.height(height);
			$form.find('.viewport').height(height);
			$form.find('.scrollbar').height(height);
			try{
				
				$form.tinyscrollbar_update('relative');
			}catch(error){}
		}
        console.log('offset', topOffset);

    },
    
    changeOnHover : function(id,url,color) {
                 var  imgId= "CALLBACK_IMG_"+id;
                 var msgId="CALLBACK_MSG_"+id;
                 $("#"+imgId).attr("src",url);
                 $("#"+msgId).css("color",color);
     }
});
