var EGAIN_CMD = 'egainCommand';
var CMD_CUST_INITIATE_VIDEO = 'custinitiatevideo';
var CMD_AGENT_INITIATE_VIDEO = 'agentinitiatevideo';
var CMD_AGENT_AUTO_REJECT_VIDEO = 'agentautoreject';
var CMD_CUST_AUTO_REJECT_VIDEO = 'custautoreject';
var messagingProperty = undefined;
var chattextClass = "chattext";
var vhtIds;
var customerIsTyping;
var agentName = "";
var agentInitiatedVideo = false;
var EGAIN_SUB_CMD = 'subcmd';
var _debug = false;
window.App = {};
window.App.pageOffset = 20;
window.App.submitSectionHeight = 56;
window.App.scrollsize = 30;
window.App.offerConstraints = new Object(); // this object store size and position for offers
var BACK_STOPPING_TRIGGERED = 'backstoppingtriggered';
var CMD_PAUSE_RESUME_VIDEO = 'pauseresumevideo';
var CMD_MUTE_UNMUTE_AUDIO = 'muteunmuteaudio';
var chatTranscript = new Object();

window.App.eGainCustomParams = {};

$(document).ready(function() {

    var language = "en";

    if (getUrlParameter('languageCode')) {
                  language = getUrlParameter('languageCode').toLowerCase();
    }

    var countryCode ="US";
    if (getUrlParameter('countryCode')) {
                  countryCode = getUrlParameter('countryCode').toUpperCase();
    }

	if (getUrlParameter('debug')) {
				_debug = getUrlParameter('debug').toLowerCase() == 'true';
	}
	if(getUrlParameter('ofrsessionid')){
		var oTop, oLeft, oHeight, oWidth;
		oTop = getUrlParameter('top');
		oLeft = getUrlParameter('left');
		oHeight = getUrlParameter('height');
		oWidth = getUrlParameter('width');
		if(oTop && oLeft)
			App.offerConstraints.position= {top:oTop,left:oLeft};
		if(oHeight && oWidth)
			App.offerConstraints.size= {width:oWidth,height:oHeight};
	}
	// Using CKEditor's language detect function to determine the system locale
	if(CKEDITOR.lang.detect(language)=='ar'){
		chattextClass = "chattextRtl";  //Right-to-Left css class name
	}
	var videochat = false;
	if (getUrlParameter('video'))
			videochat = (parseInt(getUrlParameter('video')) == 1); //videochat will evaluate to true or false based on the URL


	var _aId = getUrlParameter('aId');
	var _sId = getUrlParameter('sId');
	var _uId = getUrlParameter('uId');
	if(_aId && _sId && _uId)
		vhtIds = {aId : _aId, sId : _sId, uId:_uId};

    $.ajax({
		url : 'properties/messaging_' + language + '_' + countryCode + '.properties',
		type: 'GET',
		dataType : 'text',
		success : function(data){ messagingProperty = data;}
	});

   //Wait until the properties file to come before starting the application.
   	$.getScript('properties/chat_' + language + '_' + countryCode+ '.properties',function(){

           //Start the app.
   		document.title = L10N_DOCUMENT_TITLE;
   		resizeWindow('',eGainLiveConfig.chatLoginWindowHeight);
   		var retVal=egainLiveCustomConfigHook();
   		if(!retVal){
   			$.getScript('properties/custom_chat_' + language + '_' + countryCode
   				+ '.properties',function(){
   				setTimeout(function(){App.start();},0);
   				});
   		}
   		if(retVal)	setTimeout(function(){App.start();},0);
    });
});

$(window).unload(function(event) {
    //Logout on unload.
	App.connection.logout();
	App.cobrowse.stopCobrowseSession();
});

//Start the application
App.start = function() {
	//if messaging data is not loaded wait for it to be loaded
	if(typeof messagingProperty == 'undefined'){
		setTimeout(function(){App.start();},10);
		return;
	}

	if(App.utils.isVisitorMobileSDKApp()) {
        $('#eg-chat-header a.closechat').hide();
    }

	App.session = new App.Session();
    App.headerView = new App.HeaderView();
    App.cobrowse = new App.Cobrowse();

	//Check browser version, V10 and higher version is supported for IE 
	if(window.navigator.userAgent.indexOf("Trident") != -1) {
		//IE browser
		var tridentString = "Trident/";
		var index = window.navigator.userAgent.indexOf(tridentString);
		//echeck wheather IE version is equal or greater than IE10
		if (index != -1 && parseFloat(window.navigator.userAgent.substring(index+tridentString.length)) < 6) {
			App.messenger = new App.Messenger();
			(App.chat_wrap_view = new App.ChatWrapView()).showGenericErrorPage(L10N_UNSUPPORTED_BROWSER_VERSION);
			return;
		}
	}
	
	App.connection.initializeChat(function(json){
		var xml = json;
		var available = json.checkEligibility.responseType;
		var maskingPatterns = json.maskingPatterns.maskingPattern;
		App.isVideoChatLicensed = json.isVideoChatLicensed ? json.isVideoChatLicensed : false;
		App.isVideoChatEnabled = json.isVideoChatEnabled ? json.isVideoChatEnabled : false;
		App.isOffRecordEnabled = json.isOffRecordEnabled ? json.isOffRecordEnabled : false;
		if (maskingPatterns)
		{
			var pattern={}, patterns=[];
			for (var j=0;j<maskingPatterns.length; j++)
			{
				var pattern = maskingPatterns[j];
				pattern.compiledRegEx = new RegExp(pattern.regEx,"gi");
				patterns[j]=pattern;
			}
			App.utils.masker.setMaskingPatternData(patterns, json.htmlTagMatcherRegEx, json.htmlTagMatcherIncr);
		}
		if(available == "0"){

			setTimeout(function() {
				var EG_CALL_Q = window.EG_CALL_Q || [];
				EG_CALL_Q.push([ "send", "wb", "aac",
				                 document.URL, 101, {
					"EventName" : "ChatEntry"
				} ]);
			},2000);				

			//Creates sidebar.
			if(!App.utils.isVisitorMobile() && eGainLiveConfig.showSidebar) {

				App.chat_sidebar_view = new App.ChatSidebarView();
				App.chat_sidebar_view.render();
			}
			var useChatAttributes  = getUrlParameter('postChatAttributes') || false;
		    if(eGainLiveConfig.loginParameters.length>0) {
		    	//If autologin is not 0, then we render the login view, otherwise launch the auto login.
		    	var functionToUseForAction = eGainLiveConfig.autoLogin ? launchAutoLogin : renderLoginView;
		    	if( useChatAttributes && useChatAttributes != "false"  ) {
		    		//If we expect parameters to be posted to templates, then we call the function to receive it and call the appropriate function after that.
		    		receivePostParams( functionToUseForAction );
				} else {
					functionToUseForAction();
				}
		    } else
				(new App.LoginView()).login({});  //anonymous login
		}
		else if(available == "1") {

			if(eGainLiveConfig.useCallBackPageDuringOffHours == 'yes')
				(App.callback_view = new App.CallBackView()).render();
			else{
				 App.messenger = new App.Messenger();
				 (App.chat_wrap_view = new App.ChatWrapView()).showOffHours();
			}

		}
		else if(available == "2"){
			 (App.callback_view = new App.CallBackView()).render();
		}

	});
}

/*
* This function receive the parameters that are posted to the templates. This is done in 2 ways:
* 1.) For browsers that support cross browser cross domain post messaging(Browsers other that IE versions < 11),
* We attach a listener for messages.
* 2.) For IE versions < 11, we get the data from local storage. The data is set in the local storage by the iframe
* created in the chat code snippet.
* After parameters are received, the callback function is called with the received parameters as its argument.
*/
function receivePostParams( callbackFunction ) {
	if( window.navigator.userAgent.indexOf("Trident") == -1 ) {
		App.postMessageReceived = false;
		var domainRegex = new RegExp(eGainLiveConfig.domain);
		$(window).on( 'message', function(event) {
			if( App.postMessageReceived ) {
				return;
			} else if ( !domainRegex.test(event.originalEvent.origin) ) {
				App.postMessageReceived = true;
				App.messenger = new App.Messenger();
				(App.chat_wrap_view = new App.ChatWrapView()).showDomainMismatchError();
				event.originalEvent.source.postMessage({chatParametersReceived : true}, event.originalEvent.origin);
				return;
			} else if (event.originalEvent.data && event.originalEvent.data['eGainChatIdentifier']) {
				App.postMessageReceived = true;
				setPostedChatParameters(event.originalEvent.data);
				callbackFunction.call(this);
				event.originalEvent.source.postMessage({chatParametersReceived : true}, event.originalEvent.origin);
			}
		} );
		/* If paramters are not posted within 5 seconds, render login view and remove the listener.
		*/
		setTimeout(function() {
			if(!App.postMessageReceived) {
				$(window).off('message');
				renderLoginView();
			}
		}, 5000);
	} else {
		if(localStorage && localStorage.getItem('egainChatParameters')) {
			var egainChatParams = localStorage.getItem('egainChatParameters');
			localStorage.removeItem('egainChatParameters');
		} else if(localStorage && !localStorage.windowRefreshed && location.protocol == 'https:') {
			setTimeout(function() {
				location.reload();
			}, 100);
			localStorage.windowRefreshed = true;
		} else {
			var localStorageWindow = window.open('', 'egainChatDomainFrame');
			var egainChatParams = localStorageWindow ? localStorageWindow.getChatParameters() : null;
		}
		if(egainChatParams) {
			setPostedChatParameters(JSON.parse(egainChatParams));
		} else {
			renderLoginView();
			return;
		}
		callbackFunction.call(this);
	}
}

function setPostedChatParameters(postedChatParameters) {
	App.postedChatParameters = postedChatParameters;
}

function getPostedChatParameters() {
	return App.postedChatParameters;
}

function renderLoginView( postedChatParameters ) {
	//Render the login view.
	 resizeWindow(CONFIG_LOGIN_FORM_WIDGET_URL, eGainLiveConfig.chatLoginWindowHeight);
	 setTimeout(function(){
		(App.login = new App.LoginView()).render( null, postedChatParameters );
	},10);
}

function launchAutoLogin( postedChatParameters ) {
	( App.login = new App.LoginView() ).autologin( postedChatParameters );
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)')
                                .exec(window.location.href);
    return results ? results[1] : null;
}

function onClickOfferVideoChat(userId)
{
	App.connection.sendAcceptVideoChat(userId);
}

/**
	Customer rejects agent's offer of 'text to video chat' conversion
**/
function onClickRejectVideoChat(userId)
{
	App.connection.sendDeclineVideoChat(userId);
}
function resizeWindow(url,height)
{
	var d = getDimensions(url);
	if($.browser.msie && $.browser.version == '7.0')
		height+=30;
	else if($.browser.msie && $.browser.version == '8.0')
		height+=13;
	else if($.browser.mozilla)
			height+=8;
	if(App.offerConstraints.size){
		d.width = App.offerConstraints.size.width;
		height = App.offerConstraints.size.height;
	}
	if(App.offerConstraints.position){
		d.top = App.offerConstraints.position.top;
		d.left = App.offerConstraints.position.left;
		if(top && top.moveTo)
			top.moveTo(d.left , d.top);
	}

	if(top && top.resizeTo)
		top.resizeTo(d.width , height);
	setTimeout(function(){showSideBar(url);},0);
}

function showSideBar(url){

		if(App.chat_sidebar_view){
			if( url && url.length>0){
				var width = $(window).width() ;
				width = width/2 - App.pageOffset;
				App.chat_sidebar_view.show();
				$('#eg-chat-content').css('width',width);
				$('#eg-chat-header').width(width);
				App.chat_sidebar_view.setURL(url);
			}
		}

}
function getDimensions(url) { //this function is different from how it used to be, but I think we should require window height, width, and width with video in eGainLiveConfig to simplify things. It was always a mystery to me where the actual window sizing was taking place, so this makes it easier to understand.

	var clientWidth = eGainLiveConfig.chatWindowWidth;


	var clientTop = 0;
	if(!(CONFIG_KNOWLEDGE_BASE_URL && CONFIG_KNOWLEDGE_BASE_URL.length>0))
		clientWidth-=53;
	if (!!eGainLiveConfig.showSidebar && App.chat_sidebar_view && url && url.length > 0)
	{
		clientWidth*=2;

		if(eGainLiveConfig.chatWindowWidthWithWidget)
			clientWidth = eGainLiveConfig.chatWindowWidthWithWidget;

	}
	var clientLeft = (window.screen.availWidth - clientWidth)*98/100;
	return {width:clientWidth,left:clientLeft,top:clientTop};
}


function stopCobrowseCallback(cbSessionId)
{
	App.cobrowse.stopCobrowseCallback(cbSessionId);
}

function openCobrowseWindow(url, cbWndName, cbSessionId) {
	App.cobrowse.openCobrowseWindow(url, cbWndName, cbSessionId);
}

function decodeMessage(messageHtml){
 messageHtml = messageHtml.replace(/\^/g, '%');
 messageHtml = unescape(messageHtml);
 messageHtml = $.trim(messageHtml);
 return messageHtml;
}

function chatSave(){
	document.saveAsForm.content.value = getContent('SAVE');
				var sysdate=new Date().format("mmmdd_yyyy_hhMMTT");		//To get date in the format Aug9_2012_1020AM.htm
				var systemdate=sysdate.toString();
				document.saveAsForm.sysdate.value= systemdate;
				document.saveAsForm.target = "saveastarget";
				document.saveAsForm.submit();

}
function chatPrint() {
	var windowObject = openTranscriptWindow('PRINT');

	// Need to do this hack for Opera browser.
	var objBrowse = windowObject.navigator;
	if (objBrowse.appName == "Opera")
	{
		windowObject.onload = function () {
		    window.setTimeout(function () {
		        windowObject.print();
		    }, 500);
		};
	}
	else
		windowObject.print();
}


var titleAlertTimerId = null;

function startTitleAlert() {
	if (titleAlertTimerId != null) {
		return;
	}
	titleAlertTimerId = setInterval(function() {
		document.title = document.title == L10N_NEW_MESSAGE ? L10N_DOCUMENT_TITLE : L10N_NEW_MESSAGE;
	}, 1000);
	window.onmousemove = stopTitleAlert;
}

function stopTitleAlert() {
	if (titleAlertTimerId == null) {
		return;
	}
	clearInterval(titleAlertTimerId);
	titleAlertTimerId = null;
	document.title = L10N_DOCUMENT_TITLE;
	window.onmousemove = null;
}

function openTranscriptWindow(windowId) {
	var options = 'width=350,height=250' //
			+ ',menubar=0' //
			+ ',toolbar=1' //
			+ ',status=0'//
			+ ',scrollbars=1'//
			+ ',resizable=1';
	options = ''; // no options -> new tab
	var windowObject = window.open('', windowId, options);
	windowObject.document.open('text/html', 'replace');
	windowObject.document.writeln(getContent(windowId));
	windowObject.document.close();
	windowObject.focus();
	return windowObject;
}

function registerHandlebarsHelpers() {

Handlebars.registerHelper('ifCustomer', function(options) {
		if (!this.attributes){
			return options.inverse(this);
		}
        if (this.attributes.newType== "customer") {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

	Handlebars.registerHelper('ifAgent1', function(options) {
		if (!this.attributes){
			return options.inverse(this);
		}
        if (this.attributes.newType == "agent1") {
                return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

	Handlebars.registerHelper('ifAgent2', function(options) {
		if (!this.attributes){
			return options.inverse(this);
		}
        if (this.attributes.newType == "agent2") {
                return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

	Handlebars.registerHelper('ifAgent3', function(options) {
		if (!this.attributes){
			return options.inverse(this);
		}
        if (this.attributes.newType == "agent3") {
                return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

	Handlebars.registerHelper('ifAgent4', function(options) {
		if (!this.attributes){
			return options.inverse(this);
		}
        if (this.attributes.newType == "agent4") {
                return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

	Handlebars.registerHelper('ifSystem', function(options) {
        if (this.attributes == null) {
                return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
}

function getContent(windowId)
{
	var content = new Array();
	var messages = App.messenger.getAllMessages();

	for (var i=0; i< messages.length; i++) {
		if(messages[i].attributes)
			messages[i].attributes.author = messages[i].attributes.author.toUpperCase();
		message = messages[i];
		if(message.cbAction == "add_anchor"){
			message = hookCBAddAnchor(message);
        }else{
			message = hookCBRemoveAnchor(message);
        }
	}

	registerHandlebarsHelpers();

	var template = Handlebars.compile(chatTranscript.content);
	var transcriptStartTime = App.utils.getFormattedMessage(chat_started.toUpperCase(), [App.chatStartTime]);
	var data = {
		"title" : L10N_TRANSCRIPT_TITLE,
		"liveChat" : transcript_header_live_chat,
		"transcript" : transcript_header_transcript,
		"chatStartedAt" : transcriptStartTime,
		"footerLiveChat" : L10N_TRANSCRIPT_FOOTER_LIVE_CHAT,
		"footerByEgain" : L10N_TRANSCRIPT_FOOTER_BY_EGAIN,
		"isMailTranscript" : false,
		"messages" : messages
	};

	var rawTranscript = template(data);

	rawTranscript = rawTranscript.replace(/onclick\s*=\s*[\"\'].*?[\"\']/gi, "");
	rawTranscript = rawTranscript.replace(/<script.*?>\/script>/gi, "");
	if (windowId == 'PRINT')
	{
		rawTranscript = rawTranscript.replace(/<a.*?>/gi, "<u>");
		rawTranscript = rawTranscript.replace(/<\/a>/gi, " <\/u>");
	}

	content.push(rawTranscript);

	if (transcripts.length > 0) {
        content.push('<br/>');
        content.push('<div id="cbTranscript">');

        for (var i = 0; i < transcripts.length; i++) {
            content.push('<a name="cbAnchor_' + i + '"></a>');
            content.push(transcripts[i]);
            content.push('<br/>');
        }

        content.push('</div>');

        //remove from cache
        transcripts = new Array();
    }

	content.push('</body></html>');

	return content.join(" ");
}

var transcripts = new Array();
function transcriptCallback(result){
   transcripts.push(result);
}


function loadCBTranscriptContent(sessionId,custName){
    //add timezone information
    var tzone = getTimeZoneOffset();
    var transcriptURL = "../../../web/view/collaboration/agent/fetchTranscript.jsp?sessionId="+sessionId+"&consumer=CUSTOMER&custName="+custName+"&tzone="+tzone;
    $.ajax({
        url : transcriptURL,
        success : transcriptCallback,
        async : false
    });
}

function hookCBAddAnchor(messageIn) {
    var message = "";
    var messageOut = messageIn;

    var localAnchor = "<a href='#cbAnchor_"+ transcripts.length +"' title=\"" + L10N_VIEW_CB_TX_TOOLTIP + "\">" + messageIn.cbSession + "</a>";
    //load the transcript's
    loadCBTranscriptContent(messageIn.cbSession, messageIn.cbCustName);
    message = messageIn.body;
    messageOut.body = message.replace(messageIn.cbSession, localAnchor);

    return messageOut;
}

function hookCBRemoveAnchor(messageIn){
    //replace the href on 'Join Cobrowse Session' searching the marker
    var message = "";
    var messageOut = messageIn;

    if(messageIn.get){
        var marker = "cbAutostart=true";
        var markerPattern = new RegExp(marker, "i");

        message = messageIn.get('body');
        if (message.match(markerPattern)) {
            var anchStartPattern = /<a.*?>/i;
            var anchEndPattern = /<\/a>/i;

            message = message.replace(anchStartPattern, "");
            message = message.replace(anchEndPattern, "");
            messageOut.set('body', message);
        }
    }

    return messageOut;

}

function getTimeZoneOffset(){
        //now use the sessionID to write add href to started cobrowse  message
        //add timezone information
        var d = new Date();
        var offset = -1 * d.getTimezoneOffset();
        var sign;
        if (offset > 0) {
            sign = "%2B"; //hex code for +
        } else {
            sign = "-";
        }

        offset = Math.abs(offset);

        var minutes = offset % 60;
        if (minutes / 10 < 1) {
            minutes = "0" + minutes;
        }
        var tzone = sign + Math.floor(offset / 60) + minutes;

        return tzone;
}

function replaceHexToASCII(str) {
    //assumes each hex is double digit and starts with ^
    if (str == "") return "";
    var caretIdx = str.indexOf("^");
    if (caretIdx <= -1) return str;

    var leftStr = str.substring(0, caretIdx);
    var hexStr = str.substring(caretIdx + 1, caretIdx + 3);
    var hexToASC = String.fromCharCode(parseInt(hexStr, 16));
    var rightStr = str.substring(caretIdx + 3);
    rightStr = replaceHexToASCII(rightStr);

    return leftStr + hexToASC + rightStr;
}

// escapes html
function transcribe(msg, cssClass) {
	return "<div class='transcriptEntry "+cssClass+"'>"+msg+"</div>";
}
// no html escaping
function transcribeHtml(msg, cssClass) {
	return "<div class='"+cssClass+"'>"+msg+"</div>";
}
function transcriptEntry(identifier, messageHtml, date) {
	return '<span class="chatidentifier">' + xmlEscape(identifier) + ' </span>'
		    + '<span class="timestamp">' + date	+ '</span>' //
			+ '<span class="'+chattextClass+'" >' +  messageHtml + '</span>';
}
//This function downloads the attachment
jQuery.download = function(url){
	if(url){
		document.formAttachment.action=url;
		document.formAttachment.sid.value = App.connection.sid;
		document.formAttachment.target = "attachmenttarget";
		document.formAttachment.setCacheHeaders.value="false"
        	document.formAttachment.submit();

	};
};

function onClickAttachment( fileName){
 fileName = escape(fileName);
 fileName=decodeMessage(fileName);
 var attachmentUrl= App.connection.BOSH_SERVICE+"/getattachment/"+encodeURIComponent(fileName);
 $.download(attachmentUrl);
}

function ratekey(event, qnsId) {
	var scoreSel = event.keyCode - 48;
	if (scoreSel >= 1 && scoreSel <= 5) {
		$("#"+qnsId).raty('score', scoreSel);
	}
}
