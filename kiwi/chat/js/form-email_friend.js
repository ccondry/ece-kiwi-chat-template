App.Emailfriend = Backbone.View.extend({

    name: 'form-email_friend',

    template: _.template($('#tpl-main').html()),

    events: {

        'submit form': 'onFormSubmit',
        'click .egce-modal-close-button': 'close'
    },

    prepare: function() {

        this.render();


    },

    render: function() {
        $('#eg-chat-global-wrap1').html(this.$el);
        $('#eg-chat-global-wrap2')[0].style.display = 'table';
        this.$el.html(_.template($('#tpl-main').html()));
    },


    onFormSubmit: function(e) {


        var that = this;
        e.preventDefault();

        //Save the form object.
        var $form = $(e.target);


        /**  Field validation
         *   Validates all fields with "required" attribute
         *
         */

        var flag = true;

        $form.find(':input[requiredfield="true"]')
            .each(function(field) {

                var _field = $(this);
                if (_field.val() == '') {

                    _field.parent().parent().addClass('errorstate');
                    _field.parent().next('.errortab').fadeIn();
                    _field.css("display", "block");
                    _field.parent().css("border", "1px solid #39BFC2");
                    _field.parent().css("border-width", "1px");
                    flag = false;

                } else {
                    _field.parent().parent().removeClass('errorstate');
                    _field.parent().next('.errortab').fadeOut();
                    _field.parent().css("border", "1px solid #ccc");

                }

            });
        if (!flag) return;


        var sender_name = $form.find('input[name="sender_name"]').val();
        var sender_email = $form.find('input[name="sender_email"]').val();
        var receiver_name = $form.find('input[name="receiver_name"]').val();
        var receiver_email = $form.find('input[name="receiver_email"]').val();


        validEntry = App.deflection.validateEntry(sender_email, 'emailaddress');
        if (!validEntry) {


            $form.find('input[name="sender_email"]').parent().parent().addClass('errorstate');
            $form.find('input[name="sender_email"]').parent().next('.errortab').html('<div class="egce-arrowLeft"></div>' + _.template('<%= L10N_INVALID_ENTRY %>')()).fadeIn();
            flag = false;
        }

        if (!flag) return;


        validEntry = App.deflection.validateEntry(receiver_email, 'emailaddress');
        if (!validEntry) {


            $form.find('input[name="receiver_email"]').parent().parent().addClass('errorstate');
            $form.find('input[name="receiver_email"]').parent().next('.errortab').html('<div class="egce-arrowLeft"></div>' + _.template('<%= L10N_INVALID_ENTRY %>')()).fadeIn();
            flag = false;


        }

        if (!flag) return;




        var comments = $form.find('textarea').val();

        //Send the email
        App.deflection.send(sender_email, sender_name, receiver_email, receiver_name, comments, App.deflection.clickedArticle, this, this._onEmailSendSuccess);

    },

    close: function() {

        var modal = $('#eg-chat-email-friend-wrap');
        modal.fadeOut(400, function() {
            modal.remove();
        });
        $('#eg-chat-global-wrap2')[0].style.display = 'none';

    },

    _onEmailSendSuccess: function() {

        var html = _.template($('#tpl-success').html());

        this.$el.html(html);
    },

    _onEmailSendError: function() {

        this.$('.error').show();
    },

    onInputFocus: function(e) {

        $(e.target).val('');
    }
});