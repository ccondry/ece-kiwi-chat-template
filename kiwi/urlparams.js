$(document).ready(function() {
	var QueryString = function () {
		  // This function is anonymous, is executed immediately and
		  // the return value is assigned to QueryString!
		var query_string = {};
		  var query = window.location.search.substring(1);
		  var vars = query.split("&");
		  for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
				// If first entry with this name
			if (typeof query_string[pair[0]] === "undefined") {
			  query_string[pair[0]] = pair[1];
				// If second entry with this name
			} else if (typeof query_string[pair[0]] === "string") {
			  var arr = [ query_string[pair[0]], pair[1] ];
			  query_string[pair[0]] = arr;
				// If third or later entry with this name
			} else {
			  query_string[pair[0]].push(pair[1]);
			}
		  }
			return query_string;
		} ();
		
	console.log("querystring",QueryString);

	loop();
	var count = 0;
	function loop() {
		if(count > 200)
			return;
		if(findFields()) {
			fillForm();
		} else {
			count++;
			setTimeout(loop, 100);
		}
	}
	
	function findFields() {
		var inputs = $.find(".input-wrap");
		console.log("inputs: ",inputs);
		return inputs.length != 0;
	}
	
	function fillForm() {
		$(".input-wrap").each(function(i) {
			//console.log("test");
			var name = $(this).data("inputName");
			//console.log(name);
			if(typeof QueryString[name] !== "undefined") {
				$("input", this).val(decodeURI(QueryString[name]));
				$("textarea", this).val(decodeURI(QueryString[name]));
			}
		});
		if(typeof QueryString.submit !== "undefined" && QueryString.submit === "true") {
			//$.find(":submit").click();
			$("input[type=submit]",document).click();
		}
	}
	
});